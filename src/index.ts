import { Client } from "discord.js";
// Js modules
const ConsoleTitle = require("node-bash-title");
// Own modules
import * as config from "./config.json";
import { changePresence, startBot } from "./state";
import { getPrefix, pingUser, gitStatus } from "./commands";

const prefix = config.prefix;
const bot = new Client();

ConsoleTitle("Github bot");

bot.on("ready", () => {
  startBot(bot);
  changePresence(bot, 0);
});

bot.on("message", async (message: any) => {
  if (message.content.startsWith(prefix)) {
    var userMessage = getPrefix(message, prefix);

    switch (userMessage.toLowerCase()) {
      case "ping":
        pingUser(message);
        break;
      case "status":
        gitStatus(message);
        break;
      default:
        message.channel.send(userMessage);
        break;
    }
  }
});

const token = config.token;
bot.login(token);
